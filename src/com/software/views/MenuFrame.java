package com.software.views;

import javax.swing.*;
import java.awt.*;

public class MenuFrame extends JFrame {
    private static JLabel currentClassLabel;

    public MenuFrame() {
        // 设置窗口标题和大小
        setTitle("课堂随机点名系统");
        setSize(500, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);  // 窗口居中显示

        // 创建菜单栏
        JMenuBar menuBar = new JMenuBar();

        // 文件菜单
        JMenu fileMenu = new JMenu("文件");
        menuBar.add(fileMenu);

        // 班级管理菜单
        JMenu classMenu = new JMenu("班级管理");
        JMenuItem addClassItem = new JMenuItem("新增班级");
        JMenuItem classListItem = new JMenuItem("班级列表");
        addClassItem.addActionListener(e -> ClassManager.showAddClassFrame());
        classListItem.addActionListener(e -> ClassManager.showClassListFrame());
        classMenu.add(addClassItem);
        classMenu.add(classListItem);
        menuBar.add(classMenu);

        // 小组管理菜单
        JMenu groupMenu = new JMenu("小组管理");
        JMenuItem addGroupItem = new JMenuItem("新增小组");
        JMenuItem groupListItem = new JMenuItem("小组列表");
        addGroupItem.addActionListener(e -> checkClassSelected(() -> ClassManager.showAddGroupFrame()));
        groupListItem.addActionListener(e -> checkClassSelected(() -> ClassManager.showGroupListFrame()));
        groupMenu.add(addGroupItem);
        groupMenu.add(groupListItem);
        menuBar.add(groupMenu);

        // 学生管理菜单
        JMenu studentMenu = new JMenu("学生管理");
        JMenuItem addStudentItem = new JMenuItem("新增学生");
        JMenuItem studentListItem = new JMenuItem("学生列表");
        addStudentItem.addActionListener(e -> checkClassSelected(() -> ClassManager.showAddStudentFrame()));
        studentListItem.addActionListener(e -> checkClassSelected(() -> ClassManager.showStudentListFrame()));
        studentMenu.add(addStudentItem);
        studentMenu.add(studentListItem);
        menuBar.add(studentMenu);

        // 课堂管理菜单
        JMenu courseMenu = new JMenu("课堂管理");
        JMenuItem randomGroupItem = new JMenuItem("随机小组");
        JMenuItem randomStudentItem = new JMenuItem("随机学生");
        randomGroupItem.addActionListener(e -> checkClassSelected(() -> ClassManager.showRandomGroupFrame()));
        randomStudentItem.addActionListener(e -> checkClassSelected(() -> ClassManager.showRandomStudentFrame()));
        courseMenu.add(randomGroupItem);
        courseMenu.add(randomStudentItem);
        menuBar.add(courseMenu);

        // 设置菜单栏
        setJMenuBar(menuBar);

        // 创建主面板
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        // 中央显示 "请选择班级"
        currentClassLabel = new JLabel("请选择班级", JLabel.CENTER);
        currentClassLabel.setFont(new Font("宋体", Font.BOLD, 20));
        currentClassLabel.setForeground(Color.RED);
        panel.add(currentClassLabel, BorderLayout.CENTER);

        // 底部右对齐的标签面板
        JPanel footerPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JLabel footerLabel = new JLabel("河北师范大学软件学院出品");
        footerLabel.setFont(new Font("宋体", Font.PLAIN, 12));
        footerPanel.add(footerLabel);
        panel.add(footerPanel, BorderLayout.SOUTH);

        // 添加主面板到窗口
        add(panel);

        // 设置窗口可见
        setVisible(true);
    }

    // 检查是否选择了班级，并执行传入的操作
    private void checkClassSelected(Runnable action) {
        if (ClassManager.getCurrentClass() == null) {
            JOptionPane.showMessageDialog(this, "请先设置班级");
        } else {
            action.run();  // 如果班级已选择，执行传入的操作
        }
    }

    // 更新当前班级显示标签
    public static void updateCurrentClassLabel(String className) {
        currentClassLabel.setText("当前班级：" + className);
    }

    // 显示主菜单页面
    public static void showMenuFrame() {
        new MenuFrame();
    }
}
