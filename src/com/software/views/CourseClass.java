package com.software.views;

import java.util.UUID;

public class CourseClass {
    private String id;   // 班级ID
    private String name; // 班级名称

    public CourseClass(String name) {
        this.name = name;
        this.id = UUID.randomUUID().toString();  // 使用UUID生成唯一的班级ID
    }

    // 获取班级名称
    public String getName() {
        return name;
    }

    // 获取班级ID
    public String getId() {
        return id;
    }
}
