package com.software.views;

public class Student {
    private String id;    // 学号
    private String name;  // 姓名
    private Group group;  // 所在小组
    private String score;  // 成绩

    // 修改构造函数，添加学号参数
    public Student(String id, String name, Group group) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Group getGroup() {
        return group;
    }

    public String getScore() {
        return score;  // 返回成绩
    }
}
