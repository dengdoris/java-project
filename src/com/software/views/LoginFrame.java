package com.software.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginFrame extends JFrame {
    private JTextField idField;
    private JPasswordField pwdField;

    public LoginFrame() {
        // 设置窗口标题
        setTitle("登录系统");

        // 设置窗口大小
        setSize(300, 280);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);  // 让窗口居中显示

        // 创建主面板
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        // 创建顶部标题标签
        JLabel titleLabel = new JLabel("登录系统", JLabel.CENTER);
        titleLabel.setFont(new Font("微软雅黑", Font.BOLD, 24));
        mainPanel.add(titleLabel, BorderLayout.NORTH);

        // 创建输入面板
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(2, 2, 5, 5));
        inputPanel.setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 55));

        // 添加编号标签和输入框
        JLabel idLabel = new JLabel("编号：");
        idField = new JTextField(25);
        inputPanel.add(idLabel);
        inputPanel.add(idField);

        // 添加密码标签和输入框
        JLabel pwdLabel = new JLabel("密码：");
        pwdField = new JPasswordField(25);
        inputPanel.add(pwdLabel);
        inputPanel.add(pwdField);

        mainPanel.add(inputPanel, BorderLayout.CENTER);

        // 创建按钮面板
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 20));

        // 登录按钮
        JButton loginButton = new JButton("登录");
        loginButton.setPreferredSize(new Dimension(70, 30));
        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });

        // 取消按钮
        JButton cancelButton = new JButton("取消");
        cancelButton.setPreferredSize(new Dimension(70, 30));
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);  // 退出系统
            }
        });

        buttonPanel.add(loginButton);
        buttonPanel.add(cancelButton);

        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // 添加主面板到窗口
        add(mainPanel);

        // 设置窗口可见
        setVisible(true);
    }

    // 登录逻辑
    private void login() {
        String id = idField.getText();
        String pwd = new String(pwdField.getPassword());

        // 简单验证逻辑
        if (id.isEmpty()) {
            JOptionPane.showMessageDialog(this, "请输入编号！");
        } else if (pwd.isEmpty()) {
            JOptionPane.showMessageDialog(this, "请输入密码！");
        } else if (id.equals("admin") && pwd.equals("123456")) {
            JOptionPane.showMessageDialog(this, "登录成功！");
            new MenuFrame();  // 打开主菜单界面
            this.dispose();  // 关闭登录窗口
        } else {
            JOptionPane.showMessageDialog(this, "用户名或密码错误！");
        }
    }
}