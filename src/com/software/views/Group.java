package com.software.views;

import java.util.UUID;

public class Group {
    private String id;   // 小组ID
    private String name; // 小组名称
    private String classId; // 所属班级ID

    public Group(String name, String classId) {
        this.name = name;
        this.classId = classId;
        this.id = UUID.randomUUID().toString();  // 使用UUID生成唯一的小组ID
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getClassId() {
        return classId;
    }

    // 重写 toString 方法，使得下拉框显示小组名称
    @Override
    public String toString() {
        return name;
    }
}
